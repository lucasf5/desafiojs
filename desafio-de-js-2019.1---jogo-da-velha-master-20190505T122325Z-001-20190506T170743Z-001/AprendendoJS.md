# Aprendendo JavasScript

Durante seu aprendizado em HTML e CSS, você deve ter percebido o quão estáticas essas linguagens são. Pensando em uma forma de tornar mais dinâmica a interação entre o usuário e a página, criou-se o JavaScript. 
Não se deixe enganar, mesmo tendo JAVA no nome, JS e JAVA não possuem muitas características em comum. O nome foi adotado como uma forma de popularizar a linguagem na época, tendo em vista a fama do JAVA.

# O que é JavaScript?

Diferentemente de HTML e CSS, JavaScript realmente é uma linguagem de programação. Suas aplicações incluem desde simples mudanças de conteúdo em HTML até inteligência artificial. Porém, nosso foco será mais voltado para o desenvolvimento WEB, usando JS para personalizar nossos formulários, enviar notificações e verificar a autenticidade do conteúdo dos campos

# Programação Básica com JS

Antes de usarmos JS em toda sua glória, precisamos entender melhor suas funções básicas e como efetivamente usá-lo em uma página WEB.

## Console

Atualmente os navegadores possuem suporte para processar JavaScript, podemos interagir com esse processamento utilizando o console, então para os próximos tópicos de operações básicas vocẽ pode usá-lo apertando **F12** (ou inspecionar a página clicando com o botão direito do mouse) e indo na aba **console**

## Operações Matemáticas

O JavaScript sabe somar, então se você colocar 1+1 ele retornará 2 no console.
```js
>> 1+1
<< 2
```
INCRÍVEL. Mas não só isso, o Javascript também consegue efetuar operações com strings (strings são conjuntos de caracteres guardados de forma sequencial na memória, na prática são frases e palavras), então se eu utilizar o símbolo de concatenação (junção de strings) **+** nas strings "que " e "massa!" teremos o seguinte:
 ```js
>> "que " + "massa!"
<< "que massa!"
```

Incrível galera, lembrando também que podemos fazer isso com variáveis, futuramente vamos falar melhor sobre elas, mas pelo menos você já sabe que é possível. Continuando,

Outras operações incluem subtração, divisão e multiplicação, porém elas não funcionam para strings. Contudo, o JS é sagaz e vai tentar converter sua string em um tipo numérico, se ele não conseguir ele vai te retornar um valor NaN (Not a Number), veja os exemplos abaixo:

```js
>> "2" - "1"
<< 1
	
>> "2" * 3
<< 6
	
>> "ola" - "mundo"
<< NaN
```
Parabéns! Você acaba de aprender a fazer contas matemáticas usando JavaScript. Você está chocado?! Calma que ainda tem mais!

## Variáveis

Em JavaScript é muito fácil declarar variáveis, basta colocar um **var** antes do nome da sua variável e é isso. Diferente de outras linguagens frescas (C cof cof) você não precisa especificar o tipo da sua variável, o que for o JS aceita, porem isso torna a linguagem mais lenta dahh. Exemplo de declaração de variável abaixo:
```js
>> var x = 12
>> var y = 32.5
>> var frase = "gti vai que vai"
```
Retomando o que tinha sido dito antes, você consegue efetuar operações com essas variáveis.
```js
>> x + y
<< 44.5

>> string + x
<< "gti vai que vai12"
//perceba que ele converteu o numérico 12 
//para string e concatenou as duas

```
## Vetores

Vetores são listas de variáveis. Segue exemplo de como declarar um vetor
```js
>> var feira = ['banana', 'maçã', 'pera']
>> var bingo = [1, 2, 3] 
```
A cada um desses valores é associado um número que identifica cada um deles, o quão chamamos de **índice**. O índice indica uma posição no vetor. Segue o exemplo abaixo:
```js
>> feira[0] //o valor na posição 0 do vetor feira
<< "banana"

>> bingo[0] + bingo[1] //a soma das posições 0 e 1 no vetor bingo
<< 3
```
**ATENÇÃO** 
A partir desse ponto é extremamente recomendável que você use um editor de texto para fazer os códigos, somente via console fica difícil vizualizar esses tópicos.
```HTML
<body>
	<h1>Hello World!</h2>
<body>

<script>
CODIGO JAVASCRIPT VEM AQUI MEU CONSAGRADO(A)
</script>
```
## Loops

As estruturas de loops foram criadas com o intuito de realizar instruções até que uma certa condição seja atingida. Para fazer isso em JavaScript é muito simples:
```js
var i //variavel que registra nosso contador
for(i = 0; i< 10; i++) {
	console.log(i)
}

i=0
while(i<10){ //enquanto isso for verdade faça...
	i++
	console.log(i)
}

i=0
do { //faça...
	i++
	console.log(i)
}while(i<10) //enquanto isso for verdade
```
Algumas informações importantes sobre esse código:
**console.log()**: Essa função exibe no console o valor que está sendo passado como parâmetro, ou seja, o que está dentro dos parênteses.
**i<10**: Enquanto o valor de i for menor que 10 o loop vai continuar
**i++**: A cada iteração a variável faz a seguinte operação: i = i + 1


## Funções

Funções são blocos de operações reutilizáveis, então caso você tenha que usar as mesmas operações várias vezes, você pode fazer uma função e chamá-la sempre que precisar. As funções podem ou não retornar valores.
Exemplo, vamos dizer que em certo momento do seu sistema vocẽ realize uma operação de somar 10 e multiplicar por 2, ao invés de escrever toda vez essa operação você pode chamar uma função e facilitar sua vida.
```js
var x = 10
function nomeDaFuncao(parametro) {
	var resultado = (parametro + 10) * 2
	return resultado //quando voce chamar a 
					 //funcao ela vai retonar resultado
}

var y = nomeDaFuncao(x);
console.log(y)
<< 200 //resultado da operaçao
```

Nossa função é como uma receita de bolo, mas nosso bolo pode ter vários sabores, mas continua sendo um bolo. O que define o sabor do bolo pode ser interpretado como o parâmetro de uma função, então se eu passar chocolate como parâmetro, meu bolo vai ser de chocolate.

Agora uma explicação super complexa e muito menos didática
Quando a função recebe um parâmetro, é como se a função criasse uma variável com o nome do parâmetro que atribue o valor enviado para a função. Exemplo:
```js
function bolo(sabor){
	console.log("esse bolo é de " + sabor)
}
bolo("morango")
"esse bolo é de morango"
```

## Importando um arquivo JS para HTML
Além de usar a tag **script** você pode importar um arquivo do tipo .js e fazendo todas suas operações nesse arquivo. Ele funciona exatamente como as tags script, mas separando os arquivos você torna seu código muito mais organizado.
```HTML
<script src="js/meu-arquivo.js"></script>
```
O atributo **src** possui o caminho do arquivo.
Pronto

# Usando JS na prática

## Como o JS interage com o HTML

Agora que já sabemos as funções básicas do JavaScript, vamos ver como ele interage com nossa página HTML  e como utilizá-lo para dinamizá-la

Primeiro vamos aprender como mudar um valor de uma div no HTML

```html
<div id="exemplo">
Ola Universo
</div>
```
Como fazemos para o nosso javascript saber onde ele deve mexer no nosso HTML? Simples, nós vamos usar um objeto chamado **document**. Objetos tem funções, que são como ações dos objetos da vida real. Então se um carro fosse um objeto ele poderia ter o método acelerar, por exemplo. Pois bem, **document** é nossa pagina HTML e através dela podemos chamar seus "filhos", que são as tags, divs, classes e tudo mais que tem dentro dela.

Para chamar um elemento com um certo id no nosso HTML, usaremos o método **getElementById("id")**
```js
<script>
	document.getElementById("exemplo")
</script>
```
Dessa maneira conseguimos chamar qualquer elemento que tenha como id "exemplo", lembrando que por ser id só pode existir apenas um elemento com essa id.

Agora para trocar o valor que está dentro dessa div nós usamos o método **innerHTML**
```js
<script>
	document.getElementById("exemplo").innerHTML
</script>
```
Pronto, agora podemos dizer para esse objeto que nós encontramos qual valor ele deve ter
```js
<script>
	document.getElementById("exemplo") = "Ola mundo"
</script>
```
Claro que fazendo isso, assim que sua página carregar você nem vai ver o que tinha escrito antes, porque seu JS vai ativar e trocar bem rápido o que estava lá, mas você pode ter certeza que funciona. Então show galera, mas vamos dizer que eu queira trocar o valor apenas quando eu fizer alguma ação específica. Calma o coração que a gente já chega lá.

**Como capturar uma classe**
Caso queiramos fazer o mesmo processo com uma classe, podemos usar o método 
**GetElementsByClassName("class")[n]**
Seguinte, vocês já sabem que em uma página HTML pode ter vários elementos com a mesma classe, então se tentarmos mudar um elemento com uma classe sem especificar qual dos elementos, então mudaremos todos de uma vez. Entenda esse método como a criação de um vetor com todos os elementos com a mesma classe. 
```HTML
<div class="aviao">
 PRIMEIRA CLASSE
</div>
<div class="aviao">
 CLASSE COMERCIAL
</div>
```
Vamos dizer que esse código HTML seja um avião e queiramos colocar o Kleyton nele, aonde você acha que ele viajaria? Mas é claro que de primeira classe! Então vamos colocá-lo usando nosso JS.
```js
<script>
var  x = document.getElementsByClassName("aviao")[0]
//podemos usar uma variavel e economizar tempo
//caso fossemos usar essa classe mais de uma vez

x.innerHTML = x.innerHTML + "<br>KleytonS2"
//com isso nao apagaremos o que ja esta la dentro, 
//mas sim adicionando mais um texto ao que ja existe
</script>
```
O que você deve ter como resultado é algo como: 
```
PRIMEIRA CLASSE  
KleytonS2
CLASSE COMERCIAL
```
**LEMBRE-SE**, os itens no vetor da classe são ordenados pela ordem que eles aparecem no código e geralmente no navegador também.  


## Como o JS escuta o HTML

Quando fazemos nosso JS, não queremos que ele faça as mudanças assim que carregamos a página, porque se não, qual seria a diferença entre fazer um JS e um HTML estático comum? Pois bem, para isso nosso JS precisa "escutar" o que acontece no nosso HTML. Como podemos fazer isso??

O que faremos é chamar um método para o nosso HTML, dessa forma:
```html
<div id="teste">
tô sad :(
</div>

<button onclick="funcaoDeTeste()">
Clique-me
</button>
```

O que estamos dizendo para o nosso HTML é que ele fique prestando atenção nesse botão e quando alguém **clicar** nesse botão ele vai executar aquela função que nós vamos definir no nosso script.
```js
<script>
function funcaoDeTeste() {
	document.getElementById("teste").innerHTML = "tô feliz :D"
}
</script>
```
O que temos agora é um código que assim que clicarmos no botão acionaremos a função que vai trocar o que está escrito dentro do nosso elemento HTML. Show de bola galera!
E existem outras formas de escutar o que acontece com nossos elementos no HTML, abaixo segue o link de mais outros. Eu recomendo testar hehe.

https://www.w3schools.com/js/js_events_examples.asp

## Como é um formulário em HTML

Formulários são fáceis de fazer, pois existe uma tag chamada **form**, por meio dela é possível criar os formulários.
```HTML
<form action="/action_page.php">
</form>
```
Lembrando que nós não trataremos de PHP nesse tutorial, então ignore  o **action**, mas saiba que é onde o seu form manda as informações depois de finalizar o preenchimento.

Continuando,
Os formulários possuem vários tipos de campos de preenchimento. Selects, texts, texts-areas, buttons e etc. Vou colocar o link abaixo da documentação dos formulários caso você tenha curiosidade. No entanto, vamos focar no tipo texto.

https://www.w3schools.com/html/html_form_elements.asp
```HTML
<form action="/action_page.php">
	<p>Insira aqui</p>
	<input name="firstname"  type="text">
</form>
```
O **name** no form é uma identificação, assim como **class** e **id**

## Como utilizar os valores dos Inputs
Como podemos recuperar ou trocar o valor do input para o nosso JS poder trabalhar?
Simples, basta usarmos o método .value e dessa forma iremos recuperar o valor do campo específico.

```js
<script>
document.getElementsByName("firstname")[0].value = "Kleyton"
</script>
```
Assim como as classes, elementos podem existir vários com o mesmo nome, por isso precisamos especificar qual queremos alterar.

Vamos fazer um **input** que se caso o valor for menor que 10, ele vai avisar quando alguém tentar enviar o formulário e não permitir que a ação submit seja feita.
Como não iremos nos apronfundar na parte do tratamento do banco de dados, vamos colocar na ação a seguinte linha **javascript:void(0);** e assim não precisamos nos preocupar 
```html
<form  action="javascript:void(0);">
	<p>Coloque um valor maior que 10</p>
	<input  name="valor"  type="number">
	<input  type="submit"  id="botao"  onclick="testaMaiorQueDez()">
	<div  id="alerta"></div>
</form>
```
Usaremos essa div alerta para indicar o que queremos dizer para o usuário.
E nosso script será assim:
```js
<script>
function  testaMaiorQueDez(){
	var  valor = document.getElementsByName("valor")[0].value
	if (valor <= 10) {
		document.getElementById("alerta").innerHTML = "maior que 10 meu chapa"
	}
	else {
		document.getElementById("alerta").innerHTML = "agora sim meu chapa"
	}
}
</script>
```
O que estamos fazendo ai é muito simples, quando alguém tenta mandar um valor que é menor que 10 ele escreve dentro da div **alerta** que o valor é menor e que não vai funcionar e se o valor corresponder com o correto ele diz que agora está correto. Simples, não?

Mas também podemos alterar estilos usando JS, então vamos dizer que quando nosso querido usuário digitar um valor menor que 10 nosso campo de preenchimento fique vermelho, para indicar pra ele que vacilou.
Para isso vamos usar o método .style e depois disso vamos dizer o que queremos alterar do estilo, no caso borderColor, dessa forma teremos:
```js
document.getElementsByName("valor")[0].style.borderColor = "red"
```
Colocaremos essa linha dentro da condicional quando o nosso usuário colocar um valor menor que 10. Assim:
```js
if (valor <= 10) {
	document.getElementById("alerta").innerHTML = "maior que 10 meu chapa"
	document.getElementsByName("valor")[0].style.borderColor = "red"
}
```
Show de bola, agora você sabe como alterar um estilo através do JavaScript e também como trabalhar com valores dos formulários.



