const player1 = "X"; //primeiro jogador
const player2=  "O"; //segundo jogador
var playTime = player1; //quem é o jogador da vez
var gameOver= 0; //fim de jogo

//função para fazer jogadas
function selecionar(a) { //JOGADORES 
     if(gameOver) {return;}//checa se o jogo acabou
     for(var i=0;i<9;i++)
     {
          if(a==i)
          {   //se o botão não possui nada nele
          if(document.getElementsByClassName("casa")[i].innerHTML==""){
          if(playTime == player1){ //vez do 1 jogador
               document.getElementsByClassName("casa")[i].innerHTML = "X";//realiza a jogada do primeiro jogador
               playTime = player2;
          }else{
               document.getElementsByClassName("casa")[i].innerHTML = "O";//realiza a jogada do segundo jogador
               playTime = player1;  
                    }
inicializador (); //mostra de quem é a vez
Venceu(); // ele mostra se o jogo já acabou por vitória
}                  
}
} 
}

inicializador(); // COMEÇO
function inicializador(){
    if(gameOver) {return;}
    if(playTime == player1){//altera o indicador da vez baseado nos jogadores
         document.getElementById("indicadorDaVez").innerHTML = "X"; 
    }
    if(playTime == player2){
         document.getElementById("indicadorDaVez").innerHTML = "O"; 
    }
}

function Venceu() { //TODAS AS VARIAVEIS PARA A PARTIDA

     var a0 = document.getElementById("a0").innerHTML; // 1° variável
     var a1 = document.getElementById("a1").innerHTML; // 2° variável
     var a2 = document.getElementById("a2").innerHTML; // 3° variável

     var a3 = document.getElementById("a3").innerHTML; // 4° variável
     var a4 = document.getElementById("a4").innerHTML; // 5° variável
     var a5 = document.getElementById("a5").innerHTML; // 6° variável

     var a6 = document.getElementById("a6").innerHTML; // 7° variável
     var a7 = document.getElementById("a7").innerHTML; // 8° variável
     var a8 = document.getElementById("a8").innerHTML; // 9° variável
  
     var vencedor =""; 
		 if(((a0!="")||(a4!=""))||(a8!="")){//calcula as possibilidade da partida
     if(((a0 == a3 && a0 == a6) || (a0 == a1 && a0 == a2) || (a0 == a4 && a0 == a8))) { vencedor= a0;}
     else if(((a4 == a3 && a4 == a5) || (a4 == a1 && a4 == a7) || (a4 == a2 && a4 == a6))) { vencedor = a4;}
     else if (((a8 == a7 && a8 == a6) || (a8 == a2 && a8 == a5))) { vencedor = a8;}
		 else if(((a1!="")&&(a2!=""))&&((a3!="")&&(a5!=""))&&((a6!="")&&(a7!=""))){vencedor = "Deu Empate!";}
		 }
   if(vencedor!="") { // MOSTRA QUEM VENCEU
        gameOver = 1;
        document.getElementById("indicadorVencedor").innerHTML="Parabéns! : "+ vencedor;
   }
   
  
}

function resetar(){ //REINICIAR
     for(var i = 0; i < 9; i++){
         document.getElementsByClassName("casa")[i].innerHTML = "";
     } // APAGA QUEM VENCEU NA PARTIDA ANTERIOR
     document.getElementById("indicadorVencedor").innerHTML="";
     gameOver = 0;
     playTime = player1; //REINICIA O JOGO
     inicializador();
 }
 
