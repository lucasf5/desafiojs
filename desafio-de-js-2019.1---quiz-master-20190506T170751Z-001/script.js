
//Nessa variável estão todas as perguntas e respostas que vocês usarão no desafio
var perguntas = [
    {
        texto: 'O céu não é o limite',
        respostas: [
            { valor: 0, texto: 'Ter preguiça' },
            { valor: 1, texto: 'Quando der certo a gente faz' },
            { valor: 3, texto: 'Vamo pra cima familia' },
            { valor: 2, texto: 'Se der for viavel a gente faz' }
        ]
    },
    {
        texto: 'Apaixonados pela missão',
        respostas: [
            { valor: 0, texto: 'Projetos nada mais sao do que numeros' },
            { valor: 1, texto: 'Dinheiro é bom e é isto' },
            { valor: 3, texto: 'Se meu projeto nao ajudar meu cliente entao nao fiz nada' },
            { valor: 2, texto: 'Se eu entender o que esta no codigo entao ta show' }
        ]
    },
    {
        texto: 'Ohana',
        respostas: [
            { valor: 0, texto: 'Ta com problema? Se vira filhao' },
            { valor: 3, texto: 'Voce é meu irmao de batalha, to contigo e nao solto' },
            { valor: 2, texto: 'Voce é da gti, eu tambem entao tudo certo' },
            { valor: 1, texto: 'Fora da Gti nem olho' }
        ]
    },
    {
        texto: 'Integridade',
        respostas: [
            { valor: 0, texto: 'Furar a fila do ru' },
            { valor: 2, texto: 'farol amarelo pra mim é verde' },
            { valor: 1, texto: 'Clonar cartao de credito' },
            { valor: 3, texto: 'Sou integro' }
        ]
    },
    {
        texto: 'Ser capitão da nave',
        respostas: [
            { valor: 3, texto: 'Eu serei o lider que eu quero pra mim' },
            { valor: 0, texto: 'Eu nao sabia, entao nao fiz' },
            { valor: 1, texto: 'Ve la, depois me conta' },
            { valor: 2, texto: 'To so esperando meu estagio aprovar pra sair da GTi' }
        ]
    }
]
var questSelet = -1;//variavel de perguntas
var somaQuest=0; //variavel de soma
function mostrarQuestao() 
{
	
	
	var itemStatus=document.getElementsByName("resposta");//declara a variavel q checa as respostas
	if(itemStatus[0].checked != false || itemStatus[1].checked != false || 
        itemStatus[2].checked != false || itemStatus[3].checked != false || questSelet==-1){//verifica se ao menos um dos valores foi escolhido
        if(questSelet != -1){
        for(var i=0;i<4;i++){
         somaQuest = somaQuest + itemStatus[i].checked*perguntas[questSelet]['respostas'][i]['valor'];//faz a soma da pontuação
         }
        }
        document.getElementById('resultado').innerHTML = "";//oculta o resultado
        document.getElementById('confirmar').innerHTML = 'Próxima';//muda o nome do botao
        questSelet ++;//incrementa o seletor de questoes
	
        }                             
	if(questSelet<=4)//conta as 4 questoes
	{
	 document.getElementById('titulo').innerHTML = perguntas[questSelet]['texto'];//escreve o nome da pergunta atual
	 document.getElementById("listaRespostas").style.display = 'inline';//mostra a lista de butoes
	 for(var i = 0; i < 4; i++){//redefine os textos e valores para cada questao nova
            itemStatus[i].checked = false;//torna os valores falsos para não aparecer o resultado anterior
            itemStatus[i].value = perguntas[questSelet]['respostas'][i]['valor'];//define o valor de cada resposta
            document.getElementsByTagName('span')[i].innerHTML = perguntas[questSelet]['respostas'][i]['texto'];//escreve os textos da perguntas
            
        }
	}
	else{
        finalizarQuiz();//chama a proxima função
    }

}

function finalizarQuiz() {
    document.getElementById('titulo').innerHTML = 'RESULTADO DO QUIZ';//escreve resultado do quiz no titulo
    document.getElementById('listaRespostas').style.display = 'none';//oculta a lista de butoes
    document.getElementById('resultado').innerHTML = "O seu resultado foi: " + parseInt((somaQuest/15) * 100) + "%";//exibe o resultado
    document.getElementById('confirmar').innerHTML = 'TENTAR DE NOVO';//altera o nome do butao
    questSelet = -1; 
    somaQuest = 0;
    //recomeça o quiz
}

